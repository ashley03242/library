﻿using System;
using Library.Domain;
using Library.Tests.Base;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Library.Tests
{
    [TestClass]
    public class BookLoanServiceTests : LoanServiceTestsBase
    {
        private static Book GetTestBook()
        {
            return new Book
            {
                Id = 1,
                Name = "Alan Partridge: Nomad"
            };
        }

        private static LoanRecord GetTestLoanRecord()
        {
            return new LoanRecord
            {
                BorrowedItem = GetTestBook(),
                DateLoaned = new DateTime(2019, 8, 19)
            };
        }

        [TestMethod]
        public void SetsCorrectLoanReturnDate()
        {
            SetupNowDate(new DateTime(2019, 9, 19));

            var book = GetTestBook();
            var expectedReturnDate = new DateTime(2019, 10, 19);

            SetsCorrectLoanReturnDate(book, expectedReturnDate);
        }

        [TestMethod]
        public void CalculatesZeroFineWhenNotOverdue()
        {
            SetupNowDate(new DateTime(2019, 9, 17));

            var loanRecord = GetTestLoanRecord();
            GetsCorrectFine(loanRecord, 0);
        }

        [TestMethod]
        public void CalculatesCalculatesCorrectFineWhenOneDayOverdue()
        {
            SetupNowDate(new DateTime(2019, 9, 19));

            var loanRecord = GetTestLoanRecord();
            GetsCorrectFine(loanRecord, 0.2m);
        }

        [TestMethod]
        public void CalculatesCalculatesCorrectFineWhenFiveDaysOverdue()
        {
            SetupNowDate(new DateTime(2019, 9, 24));

            var loanRecord = GetTestLoanRecord();
            GetsCorrectFine(loanRecord, 1.2m);
        }

        [TestMethod]
        public void CalculatesCalculatesCorrectFineWhenOneYearOverdue()
        {
            SetupNowDate(new DateTime(2020, 9, 18));

            var loanRecord = GetTestLoanRecord();
            GetsCorrectFine(loanRecord, 73.2m);
        }
    }
}