﻿using System;
using Library.Domain;
using Library.Domain.Interfaces;
using Library.Services;
using Library.Services.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Library.Tests.Base
{
    public abstract class LoanServiceTestsBase
    {
        protected ILoanService LoanService;
        protected Mock<IDateTimeService> MockDateTimeService;

        [TestInitialize]
        public void Setup()
        {
            MockDateTimeService = new Mock<IDateTimeService>();
            LoanService = new LoanService(MockDateTimeService.Object);
        }

        protected void SetupNowDate(DateTime now)
        {
            MockDateTimeService.Setup(d => d.GetNow()).Returns(now);
        }

        protected void SetsCorrectLoanReturnDate(IBorrowableItem item, DateTime expectedReturnDate)
        {
            var loanRecord = LoanService.LoanItem(item);
            Assert.AreEqual(expectedReturnDate, loanRecord.DateDueBack);
        }

        protected void GetsCorrectFine(LoanRecord loanRecord, decimal expectedFine)
        {
            var fine = LoanService.GetFine(loanRecord);
            Assert.AreEqual(expectedFine, fine);
        }
    }
}