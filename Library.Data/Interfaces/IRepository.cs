﻿using System.Collections.Generic;
using Library.Domain;

namespace Library.Data.Interfaces
{
    public interface IRepository
    {
        IEnumerable<Book> GetBooks();
    }
}