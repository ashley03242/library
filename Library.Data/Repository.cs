﻿using System.Collections.Generic;
using Library.Data.Interfaces;
using Library.Domain;

namespace Library.Data
{
    public class Repository : IRepository
    {
        private readonly IEnumerable<Book> _books;

        public Repository()
        {
            _books = new List<Book>
            {
                new Book { Id = 1, Name = "Harry Potter and the Deathly Hallows" },
                new Book { Id = 2, Name = "Alan Partridge: Nomad" },
                new Book { Id = 3, Name = "You" }
            };
        }

        public IEnumerable<Book> GetBooks()
        {
            return _books;
        }
    }
}