﻿using Library.Domain;
using Library.Domain.Interfaces;
using Library.Services.Interfaces;

namespace Library.Services
{
    public class LoanService : ILoanService
    {
        private readonly IDateTimeService _dateTimeService;

        public LoanService(IDateTimeService dateTimeService)
        {
            _dateTimeService = dateTimeService;
        }

        public LoanRecord LoanItem(IBorrowableItem item)
        {
            var loadRecord = new LoanRecord
            {
                BorrowedItem = item,
                DateLoaned = _dateTimeService.GetNow()
            };

            return loadRecord;
        }

        public decimal GetFine(LoanRecord loanRecord)
        {
            var now = _dateTimeService.GetNow();

            if (loanRecord.DateDueBack.Date >= now.Date)
            {
                return 0;
            }

            var daysOverdue = (decimal) (now.Date - loanRecord.DateDueBack).TotalDays;

            return daysOverdue * loanRecord.BorrowedItem.FinePerDay;
        }
    }
}