﻿using Library.Domain;
using Library.Domain.Interfaces;

namespace Library.Services.Interfaces
{
    public interface ILoanService
    {
        LoanRecord LoanItem(IBorrowableItem item);
        decimal GetFine(LoanRecord loanRecord);
    }
}