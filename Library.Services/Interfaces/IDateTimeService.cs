﻿using System;

namespace Library.Services.Interfaces
{
    public interface IDateTimeService
    {
        DateTime GetNow();
    }
}