﻿using System;
using Library.Services.Interfaces;

namespace Library.Services
{
    public class DateTimeService : IDateTimeService
    {
        public DateTime GetNow()
        {
            return DateTime.UtcNow;
        }
    }
}