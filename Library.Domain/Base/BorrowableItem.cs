﻿using Library.Domain.Interfaces;

namespace Library.Domain.Base
{
    public abstract class BorrowableItem : IBorrowableItem
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public abstract int MaxBorrowDays { get; }
        public abstract decimal FinePerDay { get; }

        public virtual bool CanPersonLoanItem()
        {
            return true;
        }
    }
}