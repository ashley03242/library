﻿using System;
using Library.Domain.Interfaces;

namespace Library.Domain
{
    public class LoanRecord
    {
        public int Id { get; set; }

        public DateTime DateLoaned { get; set; }
        
        public IBorrowableItem BorrowedItem { get; set; }

        public DateTime DateDueBack => DateLoaned.AddDays(BorrowedItem.MaxBorrowDays);
    }
}