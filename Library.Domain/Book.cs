﻿using Library.Domain.Base;

namespace Library.Domain
{
    public class Book : BorrowableItem
    {
        public override int MaxBorrowDays => 30;
        public override decimal FinePerDay => 0.2m;
    }
}