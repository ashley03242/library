﻿namespace Library.Domain.Interfaces
{
    public interface IBorrowableItem
    {
        int Id { get; set; }
        string Name { get; set; }
        int MaxBorrowDays { get; }
        decimal FinePerDay { get; }

        bool CanPersonLoanItem();
    }
}